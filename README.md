## A simple HTML site using GitLab Pages

As a part of the Academic Work Academy Java Winter course.
It was the first time using HTML, CSS and the Bulma-Framework.

---

**Table of Contents**

- Landing Page
- First tab: "Der Bogen"
- Second tab: "Der Schussablauf"
- Tab: Impressum und Datenschutzhinweis


## About the project

Project requirements:

    - has to be online (e.g. with GitLab Pages)
    - Responsive (with Bulma)
    - valid code: No Errors in Console + W3 HTML Validator
